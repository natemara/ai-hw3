import java.util.Scanner;

public class Parser {
    public Board parse(Scanner file) {
        Cell cell;
        String line;

        int sideLength = file.nextInt();
        int groupSize = file.nextInt();

        Cell[][] board = new Cell[sideLength][sideLength];

        file.nextLine();

        for (int i = 0; i < sideLength; i++) {
            line = file.nextLine();
            for (int j = 0; j < sideLength; j++) {
                board[i][j] = new Cell(line.charAt(j), sideLength);
            }
        }

        return new Board(board, groupSize);
    }
}
