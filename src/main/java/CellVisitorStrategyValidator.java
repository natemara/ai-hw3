public class CellVisitorStrategyValidator {
    private Board board;
    private CellVisitorStrategy cellVisitorStrategy;

    public CellVisitorStrategyValidator(Board board, CellVisitorStrategy cellVisitorStrategy) {
        this.board = board;
        this.cellVisitorStrategy = cellVisitorStrategy;
    }

    public Position nextCell(Position p) {
        return validate(cellVisitorStrategy.nextCell(p));
    }

    private Position validate(Position p) {
        if (p == null || p.row < 0 || p.column < 0 || p.row > board.cells.length - 1 || p.column > board.cells.length - 1) {
            return null;
        } else {
            return p;
        }
    }
}
