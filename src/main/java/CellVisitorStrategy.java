/**
 * A {@link FunctionalInterface} that determines the visit order for cells.
 */
@FunctionalInterface
public interface CellVisitorStrategy {
    Position nextCell(Position startPosition);
}
