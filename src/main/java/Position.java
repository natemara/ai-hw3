public class Position {
    int row;
    int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public String toString() {
        return String.format("Position(row=%d, column=%d)", row, column);
    }

    public boolean equals(Object other) {
        if (other instanceof Position) {
            Position otherPosition = (Position) other;
            return otherPosition.row == row && otherPosition.column == column;
        }
        return false;
    }
}
