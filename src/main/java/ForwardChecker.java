public class ForwardChecker implements Runnable {
    private Board board;
    private Position initialPosition;
    private int valueToErase;

    public ForwardChecker(Board board, Position initialPosition, int valueToErase) {
        this.board = board;
        this.initialPosition = initialPosition;
        this.valueToErase = valueToErase;
    }

    public void run() {
        forwardCheck(pos -> new Position(pos.row + 1, pos.column));
        forwardCheck(pos -> new Position(pos.row - 1, pos.column));
        forwardCheck(pos -> new Position(pos.row, pos.column + 1));
        forwardCheck(pos -> new Position(pos.row, pos.column - 1));

        // Forward check next position in group
        forwardCheck(pos -> {
            if ((pos.column + 1) % board.GROUP_SIZE == 0) {
                if ((pos.row + 1) % board.GROUP_SIZE == 0) {
                    return null;
                } else {
                    return new Position(pos.row + 1, pos.column - board.GROUP_SIZE + 1);
                }
            } else {
                return new Position(pos.row, pos.column + 1);
            }
        });

        // Forward check previous position in group
        forwardCheck(pos -> {
            if (pos.column % board.GROUP_SIZE == 0) {
                if (pos.row % board.GROUP_SIZE == 0) {
                    return null;
                } else {
                    return new Position(pos.row - 1, pos.column + board.GROUP_SIZE - 1);
                }
            } else {
                return new Position(pos.row, pos.column - 1);
            }
        });
    }

    private void forwardCheck(CellVisitorStrategy cellVisitorStrategy) {
        CellVisitorStrategyValidator cellVisitorStrategyValidator = new CellVisitorStrategyValidator(board, cellVisitorStrategy);
        Position p = cellVisitorStrategyValidator.nextCell(initialPosition);
        Cell c;

        while (p != null) {
            c = board.getCell(p);
            c.domain.remove(valueToErase);

            p = cellVisitorStrategyValidator.nextCell(p);
        }
    }
}
