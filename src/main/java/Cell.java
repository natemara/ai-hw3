import java.util.HashSet;
import java.util.Set;

public class Cell {
    private int value;
    Set<Integer> domain;

    public Cell(char value, int limit) {
        domain = new HashSet<>();
        this.value = charToInt(value);

        for (int i = 1; i <= limit; i++) {
            domain.add(i);
        }
    }

    public Cell(int limit) {
        this('-', limit);
    }

    private int charToInt(char charToParse) {
        switch (charToParse) {
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            default:
                return -1;
        }
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isSet() {
        return value != -1;
    }

    public boolean isInconsistent() {
        return domain.size() == 0;
    }

    public String toString() {
        return isSet() ? String.format("%d", value) : "-";
    }

    public boolean isMin(Cell other) {
        return other == null || domain.size() < other.domain.size();
    }

    public Cell clone() {
        Cell c = new Cell(0);
        c.domain.addAll(domain);
        c.setValue(value);

        return c;
    }

    public boolean equals(Object other) {
        if (other instanceof Cell) {
            Cell cell = (Cell) other;
            return cell.value == value && cell.domain.equals(domain);
        }
        return false;
    }
}
