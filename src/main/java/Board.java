import java.util.Set;

public class Board {
    Cell[][] cells;
    final int GROUP_SIZE;
    int numSet = 0;

    /**
     * Creates a Board object, setting up all of the {@link Cell} pointers for each {@link Cell} on the board. This
     * assumes that groupSize^2 = rowSize
     * @param cells
     * @param groupSize
     */
    public Board(Cell[][] cells, int groupSize) {
        this.GROUP_SIZE = groupSize;

        this.cells = cells;

        removeUsedValues();
    }

    private void removeUsedValues() {
        Position position;
        Cell cell;
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                position = new Position(i, j);
                cell = getCell(position);

                if (cell.isSet()) {
                    numSet++;
                    ForwardChecker forwardChecker = new ForwardChecker(this, position, cell.getValue());
                    forwardChecker.run();
                }
            }
        }
    }

    /**
     * Deep copy constructor
     * @param other
     */
    public Board(Board other) {
        cells = new Cell[other.cells.length][other.cells.length];
        GROUP_SIZE = other.GROUP_SIZE;
        numSet = other.numSet;

        for(int i = 0; i < other.cells.length; i++) {
            for (int j = 0; j < other.cells.length; j++) {
                cells[i][j] = other.cells[i][j].clone();
            }
        }
    }

    public Position getVariablePosition() {
        Position min = null;
        Cell cell;

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                cell = cells[i][j];
                if (!cell.isSet() && (min == null || cell.isMin(getCell(min)))) {
                    min = new Position(i, j);
                }
            }
        }

        return min;

    }

    public void solve() {
        if (numSet == cells.length * cells.length) {
            System.out.print(this);
        } else {
            Position targetPosition = getVariablePosition();
            if (targetPosition == null) {
                return;
            }

            Cell target = getCell(targetPosition);
            assert target.isSet();
            Set<Integer> domain = target.domain;

            for (int i : domain) {
                Board copy = new Board(this);
                Cell var = copy.getCell(targetPosition);
                assert var.equals(target);

                var.setValue(i);
                copy.numSet++;

                ForwardChecker forwardChecker = new ForwardChecker(copy, targetPosition, i);
                forwardChecker.run();
                copy.solve();
            }
        }
    }

    Cell getCell(Position p) {
        return cells[p.row][p.column];
    }

    public String toString() {
        String toReturn = "";
        for (Cell[] row : cells) {
            for (Cell cell : row) {
                toReturn += cell;
            }
            toReturn += "\n";
        }
        return toReturn;
    }
}
